from collections import defaultdict

import sys

from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QMainWindow, QWidget, QGridLayout, QPushButton, QApplication, QLabel
from qgis._core import QgsVectorLayer, QgsProject, QgsRasterLayer, QgsMultiBandColorRenderer, QgsContrastEnhancement
from qgis._gui import QgsMapCanvas

from hubdsm.core.gdalraster import GdalRaster

from osgeo import gdal
gdal.SetConfigOption('GDAL_VRT_ENABLE_PYTHON', 'YES')
bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']

bands = defaultdict(list)
raster = GdalRaster.open(r'c:\test\chipsAllLandsat\LC08.tif')

years = list(range(2018, 2021))
months = list(range(1, 12))

with open('mean.vrt', 'w') as f:
    f.write(
    '''<VRTDataset rasterXSize="103" rasterYSize="103">
      <SRS dataAxisToSRSAxisMapping="1,2">PROJCS["WGS 84 / UTM zone 33N",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",15],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG","32633"]]</SRS>
      <GeoTransform>  3.9067500000000000e+05,  3.0000000000000000e+01,  0.0000000000000000e+00,  5.8214550000000000e+06,  0.0000000000000000e+00, -3.0000000000000000e+01</GeoTransform>
    ''')

    for band in raster.bands:
        yearmonth = band.description[12:18]
        bandName = band.description[21:]
        bands[(yearmonth, bandName)].append((raster.filename, band.number))

    vrtBandNumber = 0
    for year in years:
        for month in months:
            for bandName in bandNames:
                vrtBandNumber += 1
                yearmonth = str(year) + str(month).zfill(2)
                f.write(
rf'''
    <VRTRasterBand dataType="Int16" band="{vrtBandNumber}" subClass="VRTDerivedRasterBand">
        <Description>{yearmonth + '_' + bandName}</Description>
        <NoDataValue>-9999</NoDataValue>
''')
                for filename, sourceBandNumber in bands[yearmonth,bandName]:
                    f.write(
rf'''
        <SimpleSource>
            <SourceFilename relativeToVRT="0" shared="0">{filename}</SourceFilename>
            <SourceBand>{sourceBandNumber}</SourceBand>
            <SourceProperties RasterXSize="103" RasterYSize="103" DataType="Int32" BlockXSize="103" BlockYSize="19" />
            <SrcRect xOff="0" yOff="0" xSize="103" ySize="103" />
            <DstRect xOff="0" yOff="0" xSize="103" ySize="103" />
        </SimpleSource>
''')
                f.write(
'''
        <PixelFunctionLanguage>Python</PixelFunctionLanguage>
        <PixelFunctionType>geetemporalprofileplotting.vrtpixelfunction.mean</PixelFunctionType>
        </VRTRasterBand>
''')

    f.write(
'''
</VRTDataset>
''')

#raster.translate(filename='test.vrt', bandList=[1])
#print(GdalRaster.open('mean.vrt').readAsArray().shape)



app = QApplication(sys.argv)

#layer = QgsVectorLayer(r'C:/QGIS/apps/qgis/resources/data/world_map.gpkg|layername=countries', baseName='world_map')
#layer = QgsRasterLayer(r'mean.vrt', baseName='mean')
#QgsProject.instance().addMapLayers([layer])

mainWindow = QMainWindow()
mainWindow.resize(800, 600)
widget = QWidget()
layout = QGridLayout()

for i, year in enumerate(years):
    layout.addWidget(QLabel(str(year)), i + 1, 0)

for i, month in enumerate(months):
    layout.addWidget(QLabel(str(month).zfill(2)), 0, i + 1)


ds: gdal.Dataset = gdal.Open(r'mean.vrt')
i = -1

mapCanvases = list()

for iyear, year in enumerate(years):
    for imonth, month in enumerate(months):
        yearmonth = str(year) + str(month).zfill(2)
        for iband in range(ds.RasterCount):
            bandName = ds.GetRasterBand(iband+1).GetDescription()
            if bandName == yearmonth + '_' + 'NIR':
                redBand = iband + 1
            if bandName == yearmonth + '_' + 'SWIR1':
                greenBand = iband + 1
            if bandName == yearmonth + '_' + 'RED':
                blueBand = iband + 1

        layer = QgsRasterLayer(r'mean.vrt', baseName='mean')
        renderer: QgsMultiBandColorRenderer = layer.renderer()
        renderer.setRedBand(redBand)
        renderer.setGreenBand(greenBand)
        renderer.setBlueBand(blueBand)
        renderer.redContrastEnhancement().setMinimumValue(0)
        renderer.redContrastEnhancement().setMaximumValue(3000)
        renderer.greenContrastEnhancement().setMinimumValue(0)
        renderer.greenContrastEnhancement().setMaximumValue(3000)
        renderer.blueContrastEnhancement().setMinimumValue(0)
        renderer.blueContrastEnhancement().setMaximumValue(3000)

        QgsProject.instance().addMapLayers([layer])
        mapCanvas = QgsMapCanvas()
        mapCanvas.setLayers([layer])
        mapCanvas.setDestinationCrs(layer.crs())
        mapCanvas.setExtent(layer.extent())
        layout.addWidget(mapCanvas, iyear + 1, imonth +1)
        mapCanvases.append(mapCanvas)

def onExtentsChanged():
    extent = mapCanvases[0].extent()
    for mapCanvas in mapCanvases[1:]:
        mapCanvas.setExtent(extent)

mapCanvases[0].extentsChanged.connect(onExtentsChanged)



widget.setLayout(layout)
mainWindow.setCentralWidget(widget)
mainWindow.show()

sys.exit(app.exec_())
