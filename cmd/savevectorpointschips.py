import json
import pickle
import time
from multiprocessing.pool import Pool
import sys
from os import makedirs
from os.path import join, dirname, exists


def jobReadPointChips(args):
    import ee
    ee.Initialize()
    dirnameOutput, imageCollectionInfo, x, y, scale, fid, fieldNames, fieldAttributes = args
    dirnameFid = join(dirnameOutput, f'fid{fid}')
    print('FID:', fid, flush=True)
    point = ee.Geometry.Point(x, y)
    bbox = point.buffer(scale * 100).bounds()
    print(scale, scale*100)

    namespace = dict()
    exec(imageCollectionInfo['code'], namespace)
    imageCollection = namespace['imageCollection']

    if imageCollectionInfo['filterDate']:
        imageCollection = imageCollection.filterDate(
            imageCollectionInfo['filterDateStart'], imageCollectionInfo['filterDateEnd']
        )

    imageCollection = imageCollection.filterDate('1988-01-01', '1989-01-01')

    if imageCollectionInfo['filterMetadata']:
        imageCollection = imageCollection.filterMetadata(
            imageCollectionInfo['filterMetadataName'], imageCollectionInfo['filterMetadataOperator'],
            imageCollectionInfo['filterMetadataValue'],
        )
    bandNames = imageCollectionInfo['filterBands']
    imageCollection = imageCollection.select(bandNames)

    for bandName in bandNames:
        print(bandName)
        image = imageCollection.select(bandName).toBands().clip(bbox).updateMask(ee.Image(1))
        print(image.select(0).getDownloadURL({}))
        sample = image.sampleRectangle(bbox, [], -9999).getInfo()

        #        sample = image.sampleRectangle(bbox, [], -9999).getInfo()
        #sample = image.sampleRectangle(bbox, [], -9999).getInfo()

        print(sample)
        return


        if not exists(dirnameFid):
            makedirs(join(dirnameFid))
        with open(join(dirnameFid, f'chips.{bandName}.txt'), 'w') as outfile:
            json.dump(sample, outfile)


if __name__ == '__main__':

    filenamePkl = join(dirname(__file__), 'parameters.pkl')
    with open(filenamePkl, 'rb') as handle:
        argss, dirnameGee, numReader = pickle.load(handle)

    sys.path.append(dirnameGee)
    import ee

    ee.Initialize()

    pool = Pool(numReader)
    t0 = time.time()
    #datas = pool.map(jobReadPointChips, argss[:1])
    datas = list(map(jobReadPointChips, argss[:1]))

    print(f'Done in {int(time.time() - t0)} sec ({numReader} parallel reader)')
