import sys

from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import (QWidget, QGridLayout, QPushButton, QApplication, QLabel)
from qgis._gui import QgsMapCanvas

import numpy as np

from hubdsm.core.extent import Extent
from hubdsm.core.gdalraster import GdalRaster
from hubdsm.core.grid import Grid

gdalRaster = GdalRaster.open(r'C:\test\daily_.tif')
dates = gdalRaster.metadataItem('dates', 'TIMESERIES')
dates = dates[:92]  # fix dates!!!
names = gdalRaster.metadataItem('names', 'TIMESERIES')
print(dates)
print(len(dates))
array4d = gdalRaster.readAsArray()
_, ysize, xsize = array4d.shape

#array4d = array4d.reshape((len(names), len(dates), ysize, xsize))
array4d = array4d.reshape((len(dates), len(names), ysize, xsize))


xlabels = sorted(set([date[5:] for date in dates]))
ylabels = sorted(set([date[:4] for date in dates]))

xlabels = list()
for m in range(13):
    for d in range(32):
        date = QDate(1996, m, d)
        if not date.isValid():
            continue
        xlabels.append(date.toString('MM-dd'))

print(xlabels)
print(ylabels)

noData = -9999
array = np.full((len(names), ysize * len(ylabels), xsize * len(xlabels)), fill_value=noData)

for i, date in enumerate(dates):
    x = xlabels.index(date[5:])
    y = ylabels.index(date[:4])
    array[:, y * ysize : (y +1) * ysize, x * xsize : (x +1) * xsize] = array4d[i]

raster = GdalRaster.createFromArray(array=array, filename=r'c:\test\chipsBig.tif')
for band, name in zip(raster.bands, names):
    band.setDescription(name)
raster.setNoDataValue(noData)