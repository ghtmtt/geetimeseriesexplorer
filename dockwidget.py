# todo
#   read all points bug with daterange
#   test data download in lat/lon tiles
#


import math
from traceback import print_exc

from geetimeseriesexplorer.codeedit import CodeEdit

import pickle
import webbrowser
from collections import OrderedDict
from os import listdir, makedirs, remove
from os.path import join, dirname, exists
from tempfile import gettempdir
from typing import Tuple

from PyQt5 import QtGui
from PyQt5.QtGui import QColor, QPen, QBrush, QIcon, QTransform, QPixmap
from PyQt5.QtCore import Qt, QDateTime, QDate, QModelIndex, QSize
from PyQt5.QtWidgets import (QPlainTextEdit, QToolButton, QListWidget, QApplication, QListWidgetItem, QSpinBox,
                             QColorDialog, QComboBox, QMainWindow, QCheckBox, QDateEdit, QLineEdit,
                             QFileDialog, QDoubleSpinBox)

from qgis.PyQt import uic
from qgis._core import (QgsProject, QgsCoordinateReferenceSystem, QgsPointXY, QgsCoordinateTransform, QgsGeometry,
                        QgsFeature, QgsVectorLayer, QgsMapLayerProxyModel, QgsFields, QgsMapLayer)
from qgis._gui import (QgsDockWidget, QgsCollapsibleGroupBox, QgisInterface, QgsMapMouseEvent, QgsFeaturePickerWidget,
                       QgsMapLayerComboBox, QgsFieldExpressionWidget, QgsFieldComboBox)

import numpy as np

import geetimeseriesexplorer.site.pyqtgraph as pg
from geetimeseriesexplorer.maptool import MapTool
from geetimeseriesexplorer.runcmd import runCmd
from geetimeseriesexplorer.site.pyqtgraph.GraphicsScene.mouseEvents import MouseClickEvent
from geetimeseriesexplorer.utils import version

debug = not True


class DockWidget(QgsDockWidget):
    mPredefinedImageCollection: QComboBox
    mOpenCatalog: QToolButton
    mShowImageCollectionDescription: QToolButton
    mLoadImageCollection: QToolButton
    mImageCollectionCode: CodeEdit

    mGroupBoxCollectionEditor: QgsCollapsibleGroupBox
    mGroupBoxRoi: QgsCollapsibleGroupBox

    mMapX: QLineEdit
    mMapY: QLineEdit
    mPanToPoint: QToolButton
    mReadPointScale: QDoubleSpinBox
    mReadPoint: QToolButton
    mPointLayerBrowser: QgsMapLayerComboBox
    mPointField: QgsFieldComboBox
    mPointPicker: QgsFeaturePickerWidget
    mSaveVectorPointsProfiles: QToolButton
    # mSaveVectorPointsChips: QToolButton
    mSaveVectorPointsNumReader: QSpinBox
    mShowLine: QToolButton
    mLineSize: QSpinBox
    mShowPoint: QToolButton
    mPointSize: QSpinBox
    mShowData: QToolButton
    mFilterDate: QCheckBox
    mFilterDateStart: QDateEdit
    mFilterDateEnd: QDateEdit
    mFilterMetadata: QCheckBox
    mFilterMetadataName: QComboBox
    mFilterMetadataOperator: QComboBox
    mFilterMetadataValue: QLineEdit
    mActivateMapTool: QToolButton
    mPlotWidget: pg.PlotWidget
    mPlotLegend: QListWidget

    # Add Map Layer Group
    mAddMapType: QComboBox
    mAddMapCompositeLiveUpdate: QCheckBox
    mAddMap: QToolButton
    mAddMapCompositeStart: QDateEdit
    mAddMapCompositeSizeValue: QSpinBox
    mAddMapCompositeSizeUnit: QComboBox
    mAddMapCompositeIncrementValue: QSpinBox
    mAddMapCompositeIncrementUnit: QComboBox
    mAddMapCompositeStepBackward: QToolButton
    mAddMapCompositeStepForward: QToolButton
    mAddMapRedBand: QComboBox
    mAddMapGreenBand: QComboBox
    mAddMapBlueBand: QComboBox
    mAddMapRedMin: QLineEdit
    mAddMapGreenMin: QLineEdit
    mAddMapBlueMin: QLineEdit
    mAddMapRedMax: QLineEdit
    mAddMapGreenMax: QLineEdit
    mAddMapBlueMax: QLineEdit
    mAddMapRedReducer: QComboBox
    mAddMapGreenReducer: QComboBox
    mAddMapBlueReducer: QComboBox
    mAddMapPercentileApply: QToolButton
    mAddMapPercentileApplyShortcut: QToolButton
    mAddMapPercentileMin: QSpinBox
    mAddMapPercentileMax: QSpinBox
    mAddMapLoadDefaultRange: QToolButton
    mAddMapCompositeCrossHair: QToolButton
    mAddMapCompositeType: QComboBox
    mAddMapCompositePalette: QLineEdit

    mShowTemporalBinImages: QToolButton
    mShowFullMapCanvas: QCheckBox

    mExportFrame: QToolButton

    def __init__(self, iface: QgisInterface, parent=None):
        QgsDockWidget.__init__(self, parent)
        uic.loadUi(join(dirname(__file__), 'dockwidget.ui'), self)
        assert isinstance(iface, QgisInterface)
        self.iface = iface
        self.mapTool = MapTool(canvas=self.iface.mapCanvas())
        self.eeInitialized = False

        self._initData()
        self._initGui()
        self._initSignals()

    def _initData(self):
        self.imageCollection = None
        self.cacheItemChecked = dict()
        self.cacheItemColor = dict()
        self.oldPointState = None
        self.isNewImageCollection = True
        self.someColors = [QColor(h) for h in (
            '#FF0000', '#FFFF00', '#00EAFF', '#AA00FF', '#FF7F00', '#BFFF00', '#0095FF', '#FF00AA', '#FFD400',
            '#6AFF00', '#0040FF', '#EDB9B9', '#B9D7ED', '#E7E9B9', '#DCB9ED', '#B9EDE0', '#8F2323', '#23628F',
            '#8F6A23', '#6B238F', '#4F8F23', '#737373', '#CCCCCC',
        )] + [QColor(name) for name in QColor.colorNames() if name not in 'black']
        self.defaultVisRanges = dict()
        self.pgProfile = list()
        self.clearProfile()
        self.predefinedImageCollections = OrderedDict()
        self.predefinedBandColors = OrderedDict()

        # init plot selection bar/box
        self.dataYMin = 0.
        self.dataYMax = 1.
        self.pgBar = None
        self.pgBox = None

    def _initGui(self):
        self.setWindowTitle('{} (v{})'.format(self.windowTitle(), version()))
        self.mLoadImageCollection.setIcon(QIcon(join(dirname(__file__), 'icon_refresh.png')))
        self.initPredefinedImageCollections()
        self.itemTemplate: QListWidgetItem = self.mPlotLegend.item(0).clone()

        names = [
            'Mean', 'StdDev', 'Variance', 'Kurtosis', 'Skewness', 'First', 'Last', 'Min', 'P5', 'P10', 'P25', 'Median',
            'P75', 'P95', 'Max'
        ]
        for w in [self.mAddMapRedReducer, self.mAddMapGreenReducer, self.mAddMapBlueReducer]:
            w.addItems(names)
        self.mFilterMetadataOperator.addItems(
            ["equals", "less_than", "greater_than", "not_equals", "not_less_than", "not_greater_than", "starts_with",
             "ends_with", "not_starts_with", "not_ends_with", "contains", "not_contains"
             ]
        )

        self.mPlotWidget.showGrid(x=True, y=True, alpha=1.)

        self.mPointLayerBrowser.setLayer(None)
        self.mPointLayerBrowser.setFilters(QgsMapLayerProxyModel.PointLayer)

        self.mImageCollectionCode.setText('')
        self.mFilterDateEnd.setDate(QDateTime.currentDateTime().date())

        self.mGroupBoxCollectionEditor.setCollapsed(True)

        self.onAddMapCompositeTypeChanged()


    def _initSignals(self):
        self.mPredefinedImageCollection.currentIndexChanged.connect(self.onPredefinedIndexChanged)
        self.mLoadImageCollection.clicked.connect(self.onLoadImageCollectionClicked)
        self.mPlotLegend.itemChanged.connect(self.plotProfile)
        self.mPlotLegend.doubleClicked.connect(self.onPlotLegendDoubleClicked)
        self.mShowLine.clicked.connect(self.plotProfile)
        self.mShowPoint.clicked.connect(self.plotProfile)
        self.mLineSize.valueChanged.connect(self.plotProfile)
        self.mPointSize.valueChanged.connect(self.plotProfile)
        self.mActivateMapTool.clicked.connect(self.onActivateMapToolClicked)
        self.mShowData.clicked.connect(self.onShowDataClicked)
        self.mLoadImageCollection.clicked.connect(self.onActivateMapToolClicked)
        self.mPlotLegend.itemChanged.connect(self.onActivateMapToolClicked)
        self.mPointLayerBrowser.layerChanged.connect(self.mPointPicker.setLayer)
        self.mPointLayerBrowser.layerChanged.connect(self.mPointField.setLayer)
        self.mPointField.fieldChanged.connect(self.mPointPicker.setDisplayExpression)
        #self.mPointPicker.setDisplayExpression('"region" ')
        self.mPointPicker.featureChanged.connect(self.onPointPickerFeatureChanged)
        self.mPanToPoint.clicked.connect(self.onPanToPointClicked)
        self.mReadPoint.clicked.connect(self.onReadPointClicked)
        self.mSaveVectorPointsProfiles.clicked.connect(self.onSaveVectorPointsProfilesClicked_cmdVersion)
        # self.mSaveVectorPointsChips.clicked.connect(self.onSaveVectorPointsChipsClicked_cmdVersion)
        self.mPlotWidget.scene().sigMouseClicked.connect(self.onPlotWidgetClicked)
        self.mPlotWidget.sigRangeChanged.connect(self.onPlotWidgetRangeChanged)
        self.mShowImageCollectionDescription.clicked.connect(self.onShowDescriptionClicked)
        self.mOpenCatalog.clicked.connect(
            lambda: webbrowser.open_new('https://developers.google.com/earth-engine/datasets/catalog')
        )

        self.mAddMapPercentileApply.clicked.connect(self.onAddMapPercentileApplyClicked)
        self.mAddMapPercentileApplyShortcut.clicked.connect(self.onAddMapPercentileApplyClicked)
        self.mAddMapCompositeStepBackward.clicked.connect(self.onAddMapCompositeStepBackwardClicked)
        self.mAddMapCompositeStepForward.clicked.connect(self.onAddMapCompositeStepForwardClicked)
        self.mAddMapCompositeStart.dateChanged.connect(self.plotSelection)
        self.mAddMapCompositeSizeValue.valueChanged.connect(self.plotSelection)
        self.mAddMapCompositeSizeUnit.currentIndexChanged.connect(self.plotSelection)
        self.mAddMapCompositeIncrementValue.valueChanged.connect(self.plotSelection)
        self.mAddMapCompositeIncrementUnit.currentIndexChanged.connect(self.plotSelection)
        self.mAddMapCompositeType.currentIndexChanged.connect(self.onAddMapCompositeTypeChanged)
        self.mShowTemporalBinImages.clicked.connect(self.onShowTemporalBinImagesClicked)
        self.mAddMapCompositeLiveUpdate.toggled.connect(self.onAddMapCompositeLiveUpdateToggled)
        self.mShowFullMapCanvas.toggled.connect(self.onAddMapClicked)


    def eeInitialize(self):

        if debug:
            print('eeInitialize')

        if not self.eeInitialized:
            self.iface.messageBar().pushInfo('GEE Timeseries Explorer', 'Initialize Earth Engine.')
            import ee
            ee.Initialize()
            self.iface.messageBar().pushSuccess('GEE Timeseries Explorer', 'Earth Engine Initialized.')

        self.eeInitialized = True

    def initPredefinedImageCollections(self):
        root = join(dirname(__file__), 'predefined')
        self.predefinedImageCollections['<select collection or use code editor>'] = ''
        for name in listdir(root):
            if name.endswith('.py'):
                with open(join(root, name)) as f:
                    text = f.read()
                text = f'# {join(root, name)}\n\n' + text
                self.predefinedImageCollections[name.replace('.py', '').replace('_', ' ')] = text
        for imageCollectionId, name in [
            ('LANDSAT/LT04/C01/T1_SR', 'USGS Landsat 4 Surface Reflectance Tier 1'),
            ('LANDSAT/LT05/C01/T1_SR', 'USGS Landsat 5 Surface Reflectance Tier 1'),
            ('LANDSAT/LE07/C01/T1_SR', 'USGS Landsat 7 Surface Reflectance Tier 1'),
            ('LANDSAT/LC08/C01/T1_SR', 'USGS Landsat 8 Surface Reflectance Tier 1'),
            ('COPERNICUS/S2_SR', 'Sentinel-2 MSI: MultiSpectral Instrument, Level-2A'),
            ('MODIS/006/MCD43A4', 'MCD43A4.006 MODIS Nadir BRDF-Adjusted Reflectance Daily 500m'),
            ('MODIS/006/MOD09GQ', 'MOD09GQ.006 Terra Surface Reflectance Daily Global 250m'),
            ('MODIS/006/MOD09GA', 'MOD09GA.006 Terra Surface Reflectance Daily Global 1km and 500m'),
            ('MODIS/006/MOD09Q1', 'MOD09Q1.006 Terra Surface Reflectance 8-Day Global 250m'),
            ('MODIS/006/MOD09A1', 'MOD09A1.006 Terra Surface Reflectance 8-Day Global 500m'),
            ('NOAA/CDR/AVHRR/SR/V5', 'NOAA CDR AVHRR: Surface Reflectance, Version 5'),
            ('NOAA/VIIRS/001/VNP09GA', 'VNP09GA: VIIRS Surface Reflectance Daily 500m and 1km')
        ]:
            text = f'import ee\n\n\nimageCollection = ee.ImageCollection("{imageCollectionId}")'
            self.predefinedImageCollections[name] = text

        self.mPredefinedImageCollection.addItems(self.predefinedImageCollections.keys())

    def currentPoint(self):
        try:
            point = QgsPointXY(float(self.mMapX.text()), float(self.mMapY.text()))
        except:
            self.iface.messageBar().pushCritical('GEE Timeseries Explorer', 'Invalid point.')
            point = None
        return point

    def currentPointState(self):
        state = (
            self.currentPoint(), self.currentBandNames(),
            self.mFilterDate.isChecked(), self.mFilterDateStart.date(), self.mFilterDateEnd.date(),
            self.mFilterMetadata.isChecked(), self.mFilterMetadataName.currentText(),
            self.mFilterMetadataOperator.currentText(), self.mFilterMetadataValue.text(),
            self.mPlotLegend.selectedIndexes()
        )
        return state

    def currentCompositeDateInfo(self, inclusiveEndDate: bool) -> Tuple[QDate, QDate, int, str, int, str]:
        sizeValue = self.mAddMapCompositeSizeValue.value()
        sizeUnit = self.mAddMapCompositeSizeUnit.currentText()
        stepValue = self.mAddMapCompositeIncrementValue.value()
        stepUnit = self.mAddMapCompositeIncrementUnit.currentText()
        dateStart = self.mAddMapCompositeStart.date()
        if sizeUnit == 'Days':
            dateEnd = dateStart.addDays(sizeValue)
        elif sizeUnit == 'Months':
            dateEnd = dateStart.addMonths(sizeValue)
        elif sizeUnit == 'Years':
            dateEnd = dateStart.addYears(sizeValue)
        else:
            assert 0

        if not inclusiveEndDate:
            dateEnd = dateEnd.addDays(1)
        return dateStart, dateEnd, sizeValue, sizeUnit, stepValue, stepUnit

    def currentBandNames(self):
        bandNames = list()
        for i in range(self.mPlotLegend.count()):
            item = self.mPlotLegend.item(i)
            if item.checkState() == Qt.Checked:
                bandNames.append(item.text())
        if len(bandNames) == 0:
            self.iface.messageBar().pushWarning('GEE Timeseries Explorer', 'No bands selected.')
            bandNames = None
        return bandNames

    def currentImageCollection(
            self, filterDate: bool, filterMetadata: bool, filterBands: bool, filterPoint: bool, buffer: int = None,
            sorted=False, limit=True
    ):
        import ee

        if self.imageCollection is None:
            return None
        imageCollection = self.imageCollection

        # filter date
        if filterDate:
            if self.mFilterDate.isChecked():
                imageCollection = imageCollection.filterDate(
                    self.mFilterDateStart.date().toString('yyyy-MM-dd'),
                    self.mFilterDateEnd.date().addDays(1).toString('yyyy-MM-dd'),
                )

        # filter metadata
        if filterMetadata:
            if self.mFilterMetadata.isChecked():
                evalType = type(self.imageProperties[self.mFilterMetadataName.currentText()])
                imageCollection = imageCollection.filterMetadata(
                    self.mFilterMetadataName.currentText(), self.mFilterMetadataOperator.currentText(),
                    evalType(self.mFilterMetadataValue.text())
                )

        # filter point region
        if filterPoint:
            point = self.currentPoint()
            if point is None:
                return None
            eePoint = ee.Geometry.Point(point.x(), point.y())
            if buffer is not None:
                eePoint = eePoint.buffer(buffer)
            imageCollection = imageCollection.filterBounds(eePoint)

        # subset bands
        if filterBands:
            bandNames = self.currentBandNames()
            if bandNames is None:
                self.clearPlot()
                return None
            else:
                imageCollection = imageCollection.select(bandNames)

        if sorted:
            imageCollection = imageCollection.sort('system:time_start')

        return imageCollection

    def currentImage(self, filterPoint: bool, buffer: int = None, filterToi=True):
        import ee
        dateStart, dateEnd, *_ = self.currentCompositeDateInfo(inclusiveEndDate=False)
        imageCollection = self.currentImageCollection(
            filterDate=True, filterMetadata=True, filterBands=False, filterPoint=filterPoint, buffer=buffer
        )

        if imageCollection is None:
            return None

        if filterToi:
            imageCollection = imageCollection.filterDate(
                dateStart.toString('yyyy-MM-dd'), dateEnd.toString('yyyy-MM-dd')
            )

        if imageCollection.size().getInfo() == 0:
            return None

        reducerRed, reducerGreen, reducerBlue = self.currentReducer()
        imageRed = imageCollection.select(self.mAddMapRedBand.currentText()).reduce(reducerRed)
        imageGreen = imageCollection.select(self.mAddMapGreenBand.currentText()).reduce(reducerGreen)
        imageBlue = imageCollection.select(self.mAddMapBlueBand.currentText()).reduce(reducerBlue)
        image = ee.Image.rgb(imageRed, imageGreen, imageBlue)
        return image

    def currentReducer(self):
        import ee
        reducers = {
            'Mean': ee.Reducer.mean(), 'StdDev': ee.Reducer.stdDev(), 'Variance': ee.Reducer.variance(),
            'Kurtosis': ee.Reducer.kurtosis(), 'Skewness': ee.Reducer.skew(), 'First': ee.Reducer.firstNonNull(),
            'Last': ee.Reducer.lastNonNull(), 'Min': ee.Reducer.min(), 'P5': ee.Reducer.percentile([5]),
            'P10': ee.Reducer.percentile([10]), 'P25': ee.Reducer.percentile([25]), 'Median': ee.Reducer.median(),
            'P75': ee.Reducer.percentile([75]), 'P95': ee.Reducer.percentile([95]), 'Max': ee.Reducer.max(),
            'Count': ee.Reducer.count(),
        }

        return (
            reducers[self.mAddMapRedReducer.currentText()], reducers[self.mAddMapGreenReducer.currentText()],
            reducers[self.mAddMapBlueReducer.currentText()]
        )

    def currentVisRanges(self):
        return (
            (float(self.mAddMapRedMin.text()), float(self.mAddMapRedMax.text())),
            (float(self.mAddMapGreenMin.text()), float(self.mAddMapGreenMax.text())),
            (float(self.mAddMapBlueMin.text()), float(self.mAddMapBlueMax.text()))
        )

    def onShowTemporalBinImagesClicked(self):

        dateStart, dateEnd, *_ = self.currentCompositeDateInfo(inclusiveEndDate=False)
        imageCollection = self.currentImageCollection(
            filterDate=False, filterMetadata=True, filterBands=False, filterPoint=True
        )
        if imageCollection is None:
            return
        imageCollection = imageCollection.filterDate(
            dateStart.toString('yyyy-MM-dd'), dateEnd.toString('yyyy-MM-dd')
        )
        imageCollection = imageCollection.select([])
        info = imageCollection.getInfo()
        self._dataWindow = QMainWindow(parent=self)
        self._dataWindow.setWindowTitle('Temporal Bin Images')
        self._dataWindow.resize(QSize(800, 600))
        lines = list()
        for feature in info['features']:
            lines.append(feature['id'])
            for k in sorted(feature['properties'].keys()):
                if k not in ['system:footprint', 'bands']:
                    lines.append(f"    {k} = {str(feature['properties'][k])}")
            lines.append('\n')
        text = '\n'.join(lines)
        dataText = QPlainTextEdit(text, parent=self)
        self._dataWindow.setCentralWidget(dataText)
        self._dataWindow.show()

    def onAddMapCompositeTypeChanged(self):
        if debug:
            print('onAddMapCompositeTypeChanged')
        multibandWidgets = [
            self.label_32, self.label_33, self.label_43, self.label_45, self.label_44, self.label_46, self.label_2,
            self.label_50, self.label_51,
            self.mAddMapGreenBand, self.mAddMapBlueBand, self.mAddMapGreenMin, self.mAddMapGreenMax,
            self.mAddMapGreenReducer, self.mAddMapBlueMin, self.mAddMapBlueMax, self.mAddMapBlueReducer,
        ]
        palettedWidgets = [self.label_1, self.label_55, self.mAddMapCompositePalette]
        for w in multibandWidgets:
            w.setVisible(self.mAddMapCompositeType.currentText() == 'Multiband')
        for w in palettedWidgets:
            w.setVisible(self.mAddMapCompositeType.currentText() == 'Paletted')

    def onAddMapCompositeStepForwardClicked(self):

        emptyDateRange = True
        with GeeWaitCursor():
            while emptyDateRange:
                dateStart, dateEnd, sizeValue, sizeUnit, stepValue, stepUnit = self.currentCompositeDateInfo(
                    inclusiveEndDate=True)
                if stepUnit == 'Days':
                    newDateStart = dateStart.addDays(stepValue)
                    newDateEnd = dateEnd.addDays(stepValue)
                elif stepUnit == 'Months':
                    newDateStart = dateStart.addMonths(stepValue)
                    newDateEnd = dateEnd.addMonths(stepValue)
                elif stepUnit == 'Years':
                    newDateStart = dateStart.addYears(stepValue)
                    newDateEnd = dateEnd.addYears(stepValue)
                else:
                    assert 0
                self.mAddMapCompositeStart.setDate(newDateStart)

                dyearStart = self.utilsDateToDecimalYear(newDateStart)
                dyearEnd = self.utilsDateToDecimalYear(newDateEnd)
                emptyDateRange = np.logical_not(np.any(
                    np.logical_and(np.greater_equal(self.dyears, dyearStart), np.less(self.dyears, dyearEnd))
                ))
                if emptyDateRange:
                    if dyearStart > max(self.dyears):
                        self.onAddMapCompositeStepBackwardClicked()
                        return

        self.plotSelection()
        if self.mAddMapCompositeLiveUpdate.isChecked():
            self.onAddMapClicked()

    def onAddMapCompositeStepBackwardClicked(self):

        emptyDateRange = True
        with GeeWaitCursor():
            while emptyDateRange:
                dateStart, dateEnd, sizeValue, sizeUnit, stepValue, stepUnit = self.currentCompositeDateInfo(
                    inclusiveEndDate=True)
                if stepUnit == 'Days':
                    newDateStart = dateStart.addDays(-stepValue)
                    newDateEnd = dateEnd.addDays(-stepValue)
                elif stepUnit == 'Months':
                    newDateStart = dateStart.addMonths(-stepValue)
                    newDateEnd = dateEnd.addMonths(-stepValue)
                elif stepUnit == 'Years':
                    newDateStart = dateStart.addYears(-stepValue)
                    newDateEnd = dateEnd.addYears(-stepValue)
                else:
                    assert 0
                self.mAddMapCompositeStart.setDate(newDateStart)

                dyearStart = self.utilsDateToDecimalYear(newDateStart)
                dyearEnd = self.utilsDateToDecimalYear(newDateEnd)
                emptyDateRange = np.logical_not(np.any(
                    np.logical_and(np.greater_equal(self.dyears, dyearStart), np.less(self.dyears, dyearEnd))
                ))

                if emptyDateRange:
                    if dyearEnd < min(self.dyears):
                        self.onAddMapCompositeStepForwardClicked()
                        return

        self.plotSelection()
        if self.mAddMapCompositeLiveUpdate.isChecked():
            self.onAddMapClicked()

    def onAddMapPercentileApplyClicked(self):
        import ee
        from ee_plugin import Map
        if self.mShowFullMapCanvas.isChecked():
            filterPoint=False
        else:
            filterPoint=True
        image = self.currentImage(filterPoint=filterPoint, buffer=None)
        point = self.currentPoint()

        eeGeometry = Map.getBounds(True)

        percentageMin, percentageMax = self.mAddMapPercentileMin.value(), self.mAddMapPercentileMax.value()
        with GeeWaitCursor():
            percentiles = image.reduceRegion(
                ee.Reducer.percentile([percentageMin, percentageMax]), bestEffort=True, maxPixels=100000,
                geometry=eeGeometry,
                scale=10
            ).getInfo()
            percentiles = {k: str(v) for k, v in percentiles.items()}
        self.mAddMapRedMin.setText(percentiles[f'vis-red_p{percentageMin}'])
        self.mAddMapRedMax.setText(percentiles[f'vis-red_p{percentageMax}'])
        self.mAddMapGreenMin.setText(percentiles[f'vis-green_p{percentageMin}'])
        self.mAddMapGreenMax.setText(percentiles[f'vis-green_p{percentageMax}'])
        self.mAddMapBlueMin.setText(percentiles[f'vis-blue_p{percentageMin}'])
        self.mAddMapBlueMax.setText(percentiles[f'vis-blue_p{percentageMax}'])

        if self.mAddMapCompositeLiveUpdate.isChecked():
            self.onAddMapClicked()

    def onShowDescriptionClicked(self):

        filename = join(gettempdir(), 'GEEImageCollectionDescription.html')
        with open(filename, 'w', encoding='utf-8') as f:
            text = self.imageCollection.get('description').getInfo()
            if text is None:
                text = 'no description available'
            f.write(text)
        webbrowser.open_new_tab(filename)

    def onPlotWidgetRangeChanged(self):
        axis = self.mPlotWidget.getAxis('bottom')

    #    xmin, xmax = axis.range[0], axis.range[1]
    #    np.linspace(xmin, xmax, 5)
    #    pg.AxisItem.tickValues()
    #    axis.setTicks([[(v, str(v)) for v in range(xminticks]])

    def onPlotWidgetClicked(self, event: MouseClickEvent, dyear=None):
        if debug:
            print('DockWidget.onPlotWidgetClicked')
        if self.dyears is None:
            return
        if dyear is None:  # user clicks into the plot
            if event.button() != Qt.LeftButton:
                return
            transform: QTransform = self.mPlotWidget.plotItem.viewTransform()
            dyear, _ = transform.map(float(event.scenePos().x()), 0.)
        else:  # direct call to snap selection
            assert dyear is not None

        # setup composite date range
        decimal, year = math.modf(dyear)
        doy = int(round(decimal * 366))
        dateClicked = QDate(int(dyear), 1, 1).addDays(doy)
        _, _, sizeValue, sizeUnit, _, _ = self.currentCompositeDateInfo(inclusiveEndDate=False)
        if sizeUnit == 'Days':
            dateStart = dateClicked.addDays(-int(sizeValue / 2) - 1)
            dateEnd = dateStart.addDays(sizeValue)
        elif sizeUnit == 'Months':
            dateStart = dateClicked.addDays(-int(sizeValue * 30 / 2))
            dateEnd = dateStart.addMonths(sizeValue)
        elif sizeUnit == 'Years':
            dateStart = dateClicked.addDays(-int(sizeValue * 365 / 2))
            dateEnd = dateStart.addYears(sizeValue)
        else:
            assert 0

        # check if selected date range is empty
        dyearStart = self.utilsDateToDecimalYear(dateStart)
        dyearEnd = self.utilsDateToDecimalYear(dateEnd)
        emptyDateRange = np.logical_not(np.any(
            np.logical_and(np.greater_equal(self.dyears, dyearStart), np.less(self.dyears, dyearEnd))
        ))
        if emptyDateRange:
            # snap to nearest observation
            dyearSnapped = self.dyears[int(np.argmin(np.abs(np.subtract(self.dyears, dyear))))]
            self.onPlotWidgetClicked(event=None, dyear=dyearSnapped)
            return

        self.mAddMapCompositeStart.setDate(dateStart)
        self.plotSelection()

        self.onAddMapClicked()

    def onAddMapClicked(self):

        from ee_plugin import Map

        if not self.mAddMapCompositeLiveUpdate.isChecked():
            return

        if self.mShowFullMapCanvas.isChecked():
            filterPoint=False
        else:
            filterPoint=True
        image = self.currentImage(filterPoint=filterPoint, buffer=None)
        if image is None:
            return

        for w in [
            self.mAddMapRedMin, self.mAddMapRedMax, self.mAddMapGreenMin, self.mAddMapGreenMax, self.mAddMapBlueMin,
            self.mAddMapBlueMax
        ]:
            if w.text() == '':
                self.onAddMapPercentileApplyClicked()
                break

        (minRed, maxRed), (minGreen, maxGreen), (minBlue, maxBlue) = self.currentVisRanges()
        layerName = 'GEE Image'

        with GeeWaitCursor():
            try:
                if self.mAddMapCompositeType.currentText() == 'Multiband':
                    vizParams = {  # 'bands': image.bandNames()}#,
                        'min': (minRed, minGreen, minBlue),
                        'max': (maxRed, maxGreen, maxBlue)}
                    Map.addLayer(image, vizParams, layerName)
                elif self.mAddMapCompositeType.currentText() == 'Paletted':
                    palette = eval(self.mAddMapCompositePalette.text())
                    vizParams = {'min': minRed, 'max': maxRed, 'palette': palette}
                    Map.addLayer(image.select(0), vizParams, layerName)
                else:
                    assert 0

            except Exception as error:
                print_exc()
                self.iface.messageBar().pushCritical('GEE Timeseries Explorer', str(error))
                return

    def onAddMapCompositeLiveUpdateToggled(self):
        # toggle GEE Image visibility in layer tree
        for layer in QgsProject.instance().mapLayersByName('GEE Image'):
            node = QgsProject.instance().layerTreeRoot().findLayer(layer)
            if node:
                node.setItemVisibilityChecked(self.mAddMapCompositeLiveUpdate.isChecked())

    def onSaveVectorPointsProfilesClicked_cmdVersion(self):
        if self.currentBandNames() is None:
            return
        canceled = self._onSaveVectorPointsPreparePkl()
        if canceled:
            return
        cmdPy = join(dirname(__file__), 'cmd', 'savevectorpointsprofiles.py')
        cmd = rf'python {cmdPy}'
        with GeeWaitCursor():
            runCmd(cmd)
            self.iface.messageBar().pushSuccess('GEE Timeseries Explorer', 'Save Vector Points Profiles.')

    def onSaveVectorPointsChipsClicked_cmdVersion(self):
        if self.currentBandNames() is None:
            return
        canceled = self._onSaveVectorPointsPreparePkl()
        if canceled:
            return
        cmdPy = join(dirname(__file__), 'cmd', 'savevectorpointschips.py')
        cmd = rf'python {cmdPy}'
        with GeeWaitCursor():
            runCmd(cmd)
            self.iface.messageBar().pushSuccess('GEE Timeseries Explorer', 'Save Vector Points Chips.')

    def _onSaveVectorPointsPreparePkl(self):
        canceled = True
        filenamePkl = join(dirname(__file__), 'cmd', 'parameters.pkl')
        if exists(filenamePkl):
            remove(filenamePkl)

        layer: QgsVectorLayer = self.mPointLayerBrowser.currentLayer()
        if layer is None:
            return canceled

        if self.imageCollection is None:
            return canceled

        evalType = type(self.imageProperties[self.mFilterMetadataName.currentText()])
        imageCollectionInfo = {
            'code': self.mImageCollectionCode.text(),
            'filterDate': self.mFilterDate.isChecked(),
            'filterDateStart': self.mFilterDateStart.date().toString('yyyy-MM-dd'),
            'filterDateEnd': self.mFilterDateEnd.date().addDays(1).toString('yyyy-MM-dd'),
            'filterMetadata': self.mFilterMetadata.isChecked(),
            'filterMetadataName': self.mFilterMetadataName.currentText(),
            'filterMetadataOperator': self.mFilterMetadataOperator.currentText(),
            'filterBands': self.currentBandNames()
        }
        try:
            imageCollectionInfo['filterMetadataValue'] = evalType(self.mFilterMetadataValue.text())
        except:
            imageCollectionInfo['filterMetadataValue'] = None

        dirnameOutput = QFileDialog.getExistingDirectory(parent=self, directory=gettempdir())
        if dirnameOutput == '':
            return canceled
        scale = self.mReadPointScale.value()
        sourceCrs: QgsCoordinateReferenceSystem = layer.crs()
        destCrs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        tr = QgsCoordinateTransform(sourceCrs, destCrs, QgsProject.instance())
        fields: QgsFields = layer.fields()
        fieldNames = fields.names()
        argss = list()
        for feature in layer.getFeatures():
            assert isinstance(feature, QgsFeature)
            geometry = QgsGeometry(feature.geometry())
            geometry.transform(tr)
            destPoint: QgsPointXY = geometry.asPoint()
            argss.append(
                (
                    dirnameOutput, imageCollectionInfo, destPoint.x(), destPoint.y(), scale, feature.id(),
                    fieldNames, list(map(str, feature.attributes()))
                )
            )

        # Can't execute Pool.map inside QGIS, so we make an CMD call
        # - prepare args file

        import ee
        dirnameGee = dirname(dirname(ee.__file__))
        numReader = 50
        with open(filenamePkl, 'wb') as f:
            pickle.dump((argss, dirnameGee, numReader), f, protocol=pickle.HIGHEST_PROTOCOL)

        return not canceled

    def onReadPointClicked(self):
        if self.imageCollection is None:
            return

        currentPointState = self.currentPointState()
        if self.oldPointState == currentPointState:
            return

        if debug:
            print('onReadPointClicked')

        with GeeWaitCursor():
            point = self.currentPoint()
            self.readProfile(coords=(point.x(), point.y()))
            self.plotProfile()

        self.oldPointState = currentPointState

    def utilsTransformProjectCrsToWgs84(self, point: QgsPointXY) -> QgsPointXY:
        QgsCoordinateReferenceSystem = QgsProject.instance().crs()
        tr = QgsCoordinateTransform(
            QgsProject.instance().crs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326),
            QgsProject.instance()
        )
        geometry = QgsGeometry.fromPointXY(point)
        geometry.transform(tr)
        return geometry.asPoint()

    def utilsTransformWgs84ToProjectCrs(self, point: QgsPointXY) -> QgsPointXY:
        QgsCoordinateReferenceSystem = QgsProject.instance().crs()
        tr = QgsCoordinateTransform(
            QgsCoordinateReferenceSystem.fromEpsgId(4326),
            QgsProject.instance().crs(),
            QgsProject.instance()
        )
        geometry = QgsGeometry.fromPointXY(point)
        geometry.transform(tr)
        return geometry.asPoint()

    def utilsMsecToDate(self, msec: int):
        return QDateTime(QDate(1970, 1, 1)).addMSecs(int(msec)).date()

    def utilsDateToDecimalYear(self, date: QDate) -> float:
        return date.year() + date.dayOfYear() / 366.

    def onPanToPointClicked(self):
        point = self.utilsTransformWgs84ToProjectCrs(point=self.currentPoint())
        if point is not None:
            self.iface.mapCanvas().setCenter(point)
            self.iface.mapCanvas().refresh()
        self.onReadPointClicked()
        if self.mAddMapCompositeLiveUpdate.isChecked():
            self.onAddMapClicked()

    def onPointPickerFeatureChanged(self):
        layer: QgsVectorLayer = self.mPointLayerBrowser.currentLayer()
        sourceCrs: QgsCoordinateReferenceSystem = layer.crs()
        destCrs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        tr = QgsCoordinateTransform(sourceCrs, destCrs, QgsProject.instance())
        feature: QgsFeature = self.mPointPicker.feature()
        geometry = QgsGeometry(feature.geometry())
        geometry.transform(tr)
        destPoint: QgsPointXY = geometry.asPoint()
        self.mMapX.setText(str(destPoint.x()))
        self.mMapY.setText(str(destPoint.y()))
        self.onPanToPointClicked()
        layer.removeSelection()
        layer.select(feature.id())

    def onShowDataClicked(self):
        if self.data is not None:
            self._dataWindow = QMainWindow(parent=self)
            self._dataWindow.setWindowTitle('Temporal Profile Data')
            self._dataWindow.resize(QSize(800, 600))
            lines = list()
            for line in self.data:
                lines.append(';'.join(map(str, line)))
            text = '\n'.join(lines)
            dataText = QPlainTextEdit(text, parent=self)
            self._dataWindow.setCentralWidget(dataText)
            self._dataWindow.show()

    def onActivateMapToolClicked(self):
        self.iface.mapCanvas().setMapTool(self.mapTool)
        self.mapTool.sigClicked.connect(self.onMapCanvasClicked)

    def onMapCanvasClicked(self, event: QgsMapMouseEvent):
        if self.imageCollection is None:
            return
        sourcePoint: QgsPointXY = event.originalMapPoint()
        sourceCrs: QgsCoordinateReferenceSystem = QgsProject.instance().crs()
        destCrs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        tr = QgsCoordinateTransform(sourceCrs, destCrs, QgsProject.instance())
        geometry = QgsGeometry.fromPointXY(sourcePoint)
        geometry.transform(tr)
        destPoint: QgsPointXY = geometry.asPoint()
        self.mMapX.setText(str(destPoint.x()))
        self.mMapY.setText(str(destPoint.y()))
        self.onReadPointClicked()

        if self.mAddMapCompositeLiveUpdate.isChecked():
            self.onAddMapClicked()

    def onPredefinedIndexChanged(self, index):
        self.mImageCollectionCode.setText(list(self.predefinedImageCollections.values())[index])
        self.onLoadImageCollectionClicked()

    def onPlotLegendDoubleClicked(self, index: QModelIndex):
        bandItem = self.mPlotLegend.item(index.row())
        currentColor = bandItem.color
        color = QColorDialog.getColor(initial=currentColor, parent=self)
        if color.name() != QColor(0, 0, 0).name():
            bandItem.color = color
            pixmap = QPixmap(16, 16)
            pixmap.fill(color)
            icon = QIcon(pixmap)
            bandItem.setIcon(icon)
            self.plotProfile()

    def onLoadImageCollectionClicked(self):
        import ee
        self.clearPlot()
        self.clearProfile()
        self.isNewImageCollection = True
        self.mPlotLegend.clear()

        with GeeWaitCursor():
            self.eeInitialize()
            namespace = dict()
            code = self.mImageCollectionCode.text()
            exec(code, namespace)
            try:
                imageCollection = namespace['imageCollection']
                assert isinstance(imageCollection, ee.ImageCollection)
                self.imageCollection = imageCollection
            except:
                self.imageCollection = None
                return
            bandNames = self.imageCollection.first().bandNames().getInfo()

        colors = self.imageCollection.get('colors').getInfo()
        self.defaultVisRanges = self.imageCollection.get('vis-ranges').getInfo()
        if self.defaultVisRanges is None:
            self.defaultVisRanges = {}
        self.defaultVisRanges = {k: [str(v1), str(v2)] for k, (v1, v2) in self.defaultVisRanges.items()}

        for bandName, color in zip(bandNames, self.someColors[:len(bandNames)]):
            checkState = self.cacheItemChecked.get(bandName, Qt.Unchecked)
            if colors is not None:
                color = QColor(colors.get(bandName, color))
            color = self.cacheItemColor.get(bandName, color)
            bandItem = self.itemTemplate.clone()
            bandItem.setText(bandName)
            bandItem.setCheckState(checkState)
            bandItem.color = color
            # brush = bandItem.background()
            # brush.setColor(color)
            # bandItem.setBackground(brush)

            pixmap = QPixmap(16, 16)
            pixmap.fill(color)
            icon = QIcon(pixmap)
            bandItem.setIcon(icon)

            self.mPlotLegend.addItem(bandItem)

        for i, w in enumerate([self.mAddMapRedBand, self.mAddMapGreenBand, self.mAddMapBlueBand]):
            w.clear()
            w.addItems(bandNames)
            w.setCurrentIndex(i)

        # set default scale
        resolution = min(
            [band['crs_transform'][0] for band in self.imageCollection.first().select(0).getInfo()['bands']]
        )
        self.mReadPointScale.setValue(resolution)

        # set metadata filter names
        self.mFilterMetadataName.clear()
        self.imageProperties = self.imageCollection.first().getInfo()['properties']
        self.mFilterMetadataName.addItems(sorted(self.imageProperties.keys()))

    def clearProfile(self):

        if debug:
            print('clearProfile')

        self.data = None
        self.header = None
        self.values = None
        self.dates = None
        self.dyears = None

    def readProfile(self, coords):

        if debug:
            print('readProfile')

        import ee
        point = ee.Geometry.Point(coords)

        imageCollection = self.currentImageCollection(
            filterDate=True, filterMetadata=True, filterBands=True, filterPoint=True
        )
        if imageCollection is None:
            return

        self.clearProfile()

        # query data
        scale = self.mReadPointScale.value()
        with GeeWaitCursor():
            try:
                self.data = imageCollection.getRegion(point, scale=scale).getInfo()
            except Exception as error:
                print_exc()
                self.iface.messageBar().pushCritical('GEE Timeseries Explorer', str(error))
                return

        if len(self.data) == 1:
            self.iface.messageBar().pushWarning('GEE Timeseries Explorer', 'Empty image collection.')
            return

        self.header = self.data[0]
        self.values = np.array(self.data[1:], dtype=object).T
        timeIndex = self.header.index('time')
        self.dates = [self.utilsMsecToDate(msec)
                      for msec in self.values[timeIndex]]
        self.dyears = [self.utilsDateToDecimalYear(date) for date in self.dates]

        # replace msec with datestamp
        for values, date in zip(self.data[1:], self.dates):
            datestamp = date.toString('yyyy-MM-dd')
            values[timeIndex] = datestamp

        if self.isNewImageCollection:
            self.onPlotWidgetClicked(event=None, dyear=2099.0)

        self.isNewImageCollection = False

    def clearPlot(self):

        if debug:
            print('clearPlot')

        for plotItem in self.pgProfile:
            self.mPlotWidget.plotItem.blockSignals(True)
            self.mPlotWidget.removeItem(plotItem)
            self.mPlotWidget.plotItem.blockSignals(False)
        self.pgProfile = list()

    def plotProfile(self):

        if debug:
            print('plotProfile')

        self.clearPlot()
        if self.values is None:
            return

        self.dataYMin = np.finfo(np.float32).max / 2
        self.dataYMax = np.finfo(np.float32).min / 2
        x = np.array(self.dyears)
        for i in range(self.mPlotLegend.count()):
            item = self.mPlotLegend.item(i)
            bandName = item.text()
            self.cacheItemChecked[bandName] = item.checkState()
            if item.checkState() == Qt.Checked:
                if bandName in self.header:
                    color = item.color  # icon().pixmap().background().color()
                    y = np.array(self.values[self.header.index(bandName)], dtype=np.float32)
                    valid = np.isfinite(y)

                    # sort by time
                    xy = sorted(zip(x[valid], y[valid]), key=lambda item: item[0])
                    xSorted, ySorted = zip(*xy)
                    self.dataYMin = min(self.dataYMin, min(ySorted))
                    self.dataYMax = max(self.dataYMax, max(ySorted))

                    if self.mShowLine.isChecked():
                        plotLine: pg.PlotDataItem = self.mPlotWidget.plot(xSorted, ySorted)
                        pen = QPen(QBrush(color), self.mLineSize.value())
                        pen.setCosmetic(True)
                        plotLine.setPen(pen)
                        self.pgProfile.append(plotLine)

                    if self.mShowPoint.isChecked():
                        plotPoints: pg.PlotDataItem = self.mPlotWidget.plot(xSorted, ySorted)
                        plotPoints.setSymbol('o')  # ['t', 't1', 't2', 't3', 's', 'p', 'h', 'star', '+', 'd', 'o']
                        plotPoints.setSymbolBrush(color)
                        plotPoints.setSymbolPen(color)
                        plotPoints.setSymbolSize(self.mPointSize.value())
                        plotPoints.setPen(None)
                        self.pgProfile.append(plotPoints)

        self.plotSelection()

    def plotSelection(self):

        self.mPlotWidget.plotItem.blockSignals(True)
        if self.pgBox is not None:
            self.mPlotWidget.removeItem(self.pgBox)
        self.mPlotWidget.plotItem.blockSignals(False)

        if self.isNewImageCollection:
            return

        if self.dyears is None:
            return

        # plot box
        dateStart, dateEnd, *_ = self.currentCompositeDateInfo(inclusiveEndDate=True)
        dateEnd = dateEnd.addDays(-1)
        x = [date.year() + date.dayOfYear() / 366 for date in
             [dateStart, dateEnd, dateEnd, dateStart, dateStart]]
        y = [self.dataYMin, self.dataYMin, self.dataYMax, self.dataYMax, self.dataYMin]
        self.pgBox: pg.PlotDataItem = self.mPlotWidget.plot(x, y)
        pen = QPen(QBrush(QColor('#FF0')), 1)
        pen.setCosmetic(True)
        self.pgBox.setPen(pen)


class GeeWaitCursor(object):

    def __enter__(self):
        QApplication.setOverrideCursor(QtGui.QCursor(Qt.WaitCursor))

    def __exit__(self, exc_type, exc_value, tb):
        QApplication.restoreOverrideCursor()

# def jobReadPoint(args: Tuple['ee.ImageCollection', float, float, int]):
#    import ee
#    imageCollection, x, y, fid = args
#    print(fid, flush=True)
#    point = ee.Geometry.Point(x, y)
#    profile = imageCollection.getRegion(point, 1)
#    data = profile.getInfo()
#    return data
