import numpy as np
from hubdsm.core.gdalraster import GdalRaster

gdalRaster = GdalRaster.open(r'C:\test\daily_.tif')
dates = gdalRaster.metadataItem('dates', 'TIMESERIES')
dates = dates[:92]  # fix dates!!!
names = gdalRaster.metadataItem('names', 'TIMESERIES')
print(dates)
print(len(dates))
array4d = gdalRaster.readAsArray()
_, ysize, xsize = array4d.shape

#array4d = array4d.reshape((len(names), len(dates), ysize, xsize))
array4d = array4d.reshape((len(dates), len(names), ysize, xsize))

xlabels = sorted(set([date[5:] for date in dates]))
ylabels = sorted(set([date[:4] for date in dates]))
print(xlabels)
print(ylabels)



noData = -9999
array = np.full((len(names), ysize * len(ylabels), xsize * len(xlabels)), fill_value=noData)

for i, date in enumerate(dates):
    x = xlabels.index(date[5:])
    y = ylabels.index(date[:4])
    array[:, y * ysize : (y +1) * ysize, x * xsize : (x +1) * xsize] = array4d[i]


raster = GdalRaster.createFromArray(array=array, filename=r'c:\test\chips.tif')
for band, name in zip(raster.bands, names):
    band.setDescription(name)
raster.setNoDataValue(noData)