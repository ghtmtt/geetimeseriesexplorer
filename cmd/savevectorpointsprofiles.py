import pickle
import time
import traceback
from multiprocessing import Manager
from multiprocessing.pool import Pool


import sys
from os.path import join, dirname, exists

from PyQt5.QtCore import QDateTime, QDate


def jobReadPointProfile(args):
    import ee
    ee.Initialize()
    dirnameOutput, imageCollectionInfo, x, y, scale, fid, fieldNames, fieldAttributes, event = args
    filenameFid = join(dirnameOutput, f'profile.fid{fid}.txt')
    filenameFailedFid = join(dirnameOutput, f'profile.failed.fid{fid}.txt')

    if exists(filenameFid):
        return

    if event.is_set():
        return

    print('FID:', fid, flush=True)
    point = ee.Geometry.Point(x, y)

    namespace = dict()
    exec(imageCollectionInfo['code'], namespace)
    imageCollection = namespace['imageCollection']
    if imageCollectionInfo['filterDate']:
        imageCollection = imageCollection.filterDate(
            imageCollectionInfo['filterDateStart'], imageCollectionInfo['filterDateEnd']
        )
    if imageCollectionInfo['filterMetadata']:
        imageCollection = imageCollection.filterMetadata(
            imageCollectionInfo['filterMetadataName'], imageCollectionInfo['filterMetadataOperator'],
            imageCollectionInfo['filterMetadataValue'],
        )
    imageCollection = imageCollection.select(imageCollectionInfo['filterBands'])

    try:
        profile = imageCollection.getRegion(point, scale)
        data = profile.getInfo()
    except:
        event.set()
        with open(filenameFailedFid, 'w') as f:
            f.write(traceback.format_exc())
            return

    with open(filenameFid, 'w') as f:
        header = data[0] + fieldNames
        tindex = header.index('time')
        f.write(';'.join(header))
        f.write('\n')
        for line in data[1:]:
            msecs = int(line[tindex])
            date = QDateTime(QDate(1970, 1, 1)).addMSecs(msecs).toString('yyyy-MM-dd')
            line[tindex] = date
            f.write(';'.join(map(str, line + fieldAttributes)))
            f.write('\n')


if __name__ == '__main__':
    filenamePkl = join(dirname(__file__), 'parameters.pkl')
    with open(filenamePkl, 'rb') as handle:
        argss, dirnameGee, numReader = pickle.load(handle)

    sys.path.append(dirnameGee)
    import ee

    ee.Initialize()

    # skip existing fids and add event for error handling
    event = Manager().Event()
    argssFiltered = list()
    for args in argss:
        dirnameOutput, imageCollectionInfo, x, y, scale, fid, fieldNames, fieldAttributes = args
        dirnameFid = join(dirnameOutput, f'fid{fid}')
        if exists(dirnameFid):
            print(f'skip {fid}')
            continue
        argssFiltered.append(args + (event, ))


    pool = Pool(numReader)
    t0 = time.time()
    datas = pool.map(jobReadPointProfile, argssFiltered)
    print(f'Done in {int(time.time() - t0)} sec ({numReader} parallel reader)')
